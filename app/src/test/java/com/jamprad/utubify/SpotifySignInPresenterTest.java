package com.jamprad.utubify;

import com.jamprad.utubify.data.DataManager;
import com.jamprad.utubify.ui.signin.AuthMvpView;
import com.jamprad.utubify.ui.signin.SpotifyAuthPresenter;
import com.jamprad.utubify.util.RxSchedulersOverrideRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by jamprad on 2016-10-02.
 */

@RunWith(MockitoJUnitRunner.class)
public class SpotifySignInPresenterTest {

    @Mock
    AuthMvpView mMockSignInMvpView;
    @Mock
    DataManager mMockDataManager;
    private SpotifyAuthPresenter mSigninPresenter;

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();

    @Before
    public void setUp() {
        mSigninPresenter = new SpotifyAuthPresenter();
        mSigninPresenter.attachView(mMockSignInMvpView);
    }

    @After
    public void tearDown() {
        mSigninPresenter.detachView();
    }

    @Test
    public void signInSucceeds() {
//        Name name = Name.create("Julian", "Pradinuk");
//        when(mMockDataManager.signIn("jampradinuk@gmail.com"))
//                .thenReturn(Single.just(name));
//        mSigninPresenter.signInGoogle();
//        verify(mMockSignInMvpView).showSuccess(name);
//        verify(mMockSignInMvpView, never()).showError();
    }

    @Test
    public void signInFails() {
//        when(mMockDataManager.signIn(""))
//                .thenReturn(Single.<Name>error(new RuntimeException()));
//        mSigninPresenter.signInGoogle();
//        verify(mMockSignInMvpView).showError();
//        verify(mMockSignInMvpView, never()).showSuccess(Mockito.any(Name.class));
    }
}
