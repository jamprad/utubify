package com.jamprad.utubify.injection.component;

import com.jamprad.utubify.injection.PerActivity;
import com.jamprad.utubify.injection.module.ActivityModule;
import com.jamprad.utubify.ui.main.MainActivity;

import dagger.Subcomponent;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity activity);
}
