package com.jamprad.utubify.injection.module;

import android.app.Application;
import android.content.Context;

import com.jamprad.utubify.data.remote.FacebookService;
import com.jamprad.utubify.data.remote.GoogleService;
import com.jamprad.utubify.data.remote.SpotifyService;
import com.jamprad.utubify.data.remote.UtubifyService;
import com.jamprad.utubify.data.remote.YouTubeService;
import com.jamprad.utubify.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    UtubifyService provideUtubify() {return new UtubifyService(mApplication);}

    @Provides
    @Singleton
    FacebookService provideFacebook() {return new FacebookService(mApplication);}

    @Provides
    @Singleton
    GoogleService provideGoogle() {return new GoogleService(mApplication);}

    @Provides
    @Singleton
    SpotifyService provideSpotify() {return new SpotifyService(mApplication);}

    @Provides
    @Singleton
    YouTubeService provideYoutube() {return new YouTubeService(mApplication);}
}
