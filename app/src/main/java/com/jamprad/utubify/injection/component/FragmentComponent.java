package com.jamprad.utubify.injection.component;

import com.jamprad.utubify.injection.PerFragment;
import com.jamprad.utubify.injection.module.FragmentModule;
import com.jamprad.utubify.ui.player.PlayerControlsFragment;
import com.jamprad.utubify.ui.player.PlayerFragment;
import com.jamprad.utubify.ui.signin.FacebookAuthFragment;
import com.jamprad.utubify.ui.signin.GoogleAuthFragment;
import com.jamprad.utubify.ui.signin.SpotifyAuthFragment;

import dagger.Subcomponent;

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {
    void inject(FacebookAuthFragment fragment);
    void inject(GoogleAuthFragment fragment);
    void inject(SpotifyAuthFragment fragment);
    void inject(PlayerFragment fragment);
    void inject(PlayerControlsFragment fragment);
}
