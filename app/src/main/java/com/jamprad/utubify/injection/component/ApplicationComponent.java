package com.jamprad.utubify.injection.component;

        import android.app.Application;
        import android.content.Context;

        import com.jamprad.utubify.data.DataManager;
        import com.jamprad.utubify.data.SyncService;
        import com.jamprad.utubify.data.local.DatabaseHelper;
        import com.jamprad.utubify.data.local.PreferencesHelper;
        import com.jamprad.utubify.data.remote.FacebookService;
        import com.jamprad.utubify.data.remote.GoogleService;
        import com.jamprad.utubify.data.remote.SpotifyService;
        import com.jamprad.utubify.data.remote.UtubifyService;
        import com.jamprad.utubify.data.remote.YouTubeService;
        import com.jamprad.utubify.injection.ApplicationContext;
        import com.jamprad.utubify.injection.module.ApplicationModule;
        import com.jamprad.utubify.util.RxEventBus;

        import javax.inject.Singleton;

        import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);
    void inject(UtubifyService utubifyService);

    @ApplicationContext Context context();
    Application application();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();
    UtubifyService utubify();
    SpotifyService spotify();
    YouTubeService youtube();
    GoogleService google();
    FacebookService facebook();
}
