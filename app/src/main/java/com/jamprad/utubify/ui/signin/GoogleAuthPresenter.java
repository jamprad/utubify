package com.jamprad.utubify.ui.signin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.jamprad.utubify.data.remote.GoogleService;
import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.injection.ConfigPersistent;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by jamprad on 2016-10-02.
 */
@ConfigPersistent
public class GoogleAuthPresenter extends AuthPresenter {

    @Inject
    GoogleService mGoogle;

    @Inject
    public GoogleAuthPresenter(){
    }

    @Override
    public void signIn(FragmentActivity activity) {
        checkViewAttached();
        mGoogle.signIn(activity);
    }

    @Override
    public Observable<AuthResult> authenticate(@NonNull FragmentActivity activity) { // TODO Review, not used
        checkViewAttached();
        return mGoogle.authenticate(activity);
    }

    @Override
    public void logout() {
        mGoogle.logout();
    }

    @Override
    public void signInResult(int requestCode, int resultCode, Intent data) {
        checkViewAttached();
        mGoogle.signInResult(requestCode, resultCode, data);
    }
}
