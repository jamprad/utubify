package com.jamprad.utubify.ui.player;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;
import android.widget.ViewSwitcher;

import com.google.api.services.youtube.model.PlaylistItem;
import com.jamprad.utubify.R;
import com.jamprad.utubify.data.model.SpotifyTrack;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.data.model.YouTubeTrack;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kaaes.spotify.webapi.android.models.ArtistSimple;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.TrackHolder> {
    private List<Track> mTracks;
    private int selectedPosition = -1;

//    @Inject
//    Fragment mFragment;

    @Inject
    PlayerPresenter mPlayerPresenter;

    @Inject
    public PlayerAdapter() {
        mTracks = new ArrayList<>();
    }

    public void addTracks(List<Track> tracks){
        if (tracks == null) return;

        boolean notify = false;
        for (Track track : tracks){
            if (mTracks.contains(track)) continue;
            notify |= mTracks.add(track);
        }
        if (notify) notifyDataSetChanged();
    }

    @Override
    public TrackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        TrackHolder trackHolder = null;
        switch(Track.Service.values()[viewType]){
            case SPOTIFY:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_spotify, parent, false);
                trackHolder = new SpotifyTrackHolder(itemView);
                break;

            case YOUTUBE:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_youtube, parent, false);
                trackHolder = new YouTubeTrackHolder(itemView);
        }
        return trackHolder;
    }

    @Override
    public int getItemViewType(int position){
        Track track = mTracks.get(position);
        return (track.service.ordinal());
    }

    @Override
    public void onBindViewHolder(TrackHolder holder, int position) {
        Track track = mTracks.get(position);

        holder.setTrack(track);
        if (holder instanceof SpotifyTrackHolder){
            //customizations for Spotify
            SpotifyTrack spotify = (SpotifyTrack) track;

            holder.mTitleView.setText(spotify.track.name);

            ArrayList<String> artists = new ArrayList<>();
            for(ArtistSimple artist : spotify.track.artists){
                artists.add(artist.name);
            }
            holder.mArtistView.setText(Arrays.toString(artists.toArray()));
        }

        else if (holder instanceof YouTubeTrackHolder){
            //customizations for YouTube
            YouTubeTrack youtube = (YouTubeTrack) track;
            PlaylistItem playlistItem = youtube.playlistItem;
            holder.mTitleView.setText((playlistItem.getSnippet().getTitle()));
            holder.mTitleView.setText(playlistItem.getSnippet().getTitle());
        }

        holder.itemView.setSelected(selectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return mTracks.size();
    }

    public int getPosition(Track track) {
        return mTracks.indexOf(track);
    }

    public void play() {
        if (selectedPosition >= 0) {
            mPlayerPresenter.play(mTracks.get(selectedPosition));
        }
    }

    public void next() {
        if (selectedPosition < mTracks.size() - 1) {
            notifyItemChanged(selectedPosition);
            selectedPosition += 1;
            notifyItemChanged(selectedPosition);
            mPlayerPresenter.play(mTracks.get(selectedPosition));
        }
    }

    public void previous() {
        if (selectedPosition > 1) {
            notifyItemChanged(selectedPosition);
            selectedPosition -= 1;
            notifyItemChanged(selectedPosition);
            mPlayerPresenter.play(mTracks.get(selectedPosition));
        }
    }


    private class YouTubeTrackHolder extends TrackHolder {
        public YouTubeTrackHolder(View itemView) {
            super(itemView);
        }
    }


    private class SpotifyTrackHolder extends TrackHolder {
        public SpotifyTrackHolder(View itemView) {
            super(itemView);
        }
    }

    class TrackHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_name)
        TextView mTitleView; //title in the case of YouTube

        @BindView(R.id.text_artist)
        TextView mArtistView; //channelTitle in the case of YouTube

        @BindView(R.id.artwork_switcher)
        PlayerArtworkSwitcher mArtworkSwitcher;

        protected Track mTrack;

        @OnClick(R.id.track)
        public void onTrackClicked(View view){
            notifyItemChanged(selectedPosition);
            selectedPosition = getLayoutPosition();
            mPlayerPresenter.play(mTrack);
            notifyItemChanged(selectedPosition);
        }

        private ViewSwitcher.ViewFactory factory = new ViewSwitcher.ViewFactory(){
            @Override
            public View makeView() {
                ImageView artwork = new ImageView(itemView.getContext());
                artwork.setScaleType(ImageView.ScaleType.FIT_CENTER);
                artwork.setLayoutParams(new ImageSwitcher.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                        Toolbar.LayoutParams.MATCH_PARENT));

                Picasso.with(itemView.getContext()).load(mTrack.getAlbumArtwork()).into(artwork);
                return artwork;
            }
        };

        public TrackHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setTrack(Track track){
            mTrack = track;
            mArtworkSwitcher.removeAllViews();
            mArtworkSwitcher.setFactory(factory);
        }
    }
}
