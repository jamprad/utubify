package com.jamprad.utubify.ui.player;

import com.jamprad.utubify.data.model.Player;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.ui.base.MvpView;

import java.util.List;

/**
 * Created by jamprad on 2016-10-02.
 */

public interface PlayerMvpView extends MvpView, Player {
    void showTracks(List<Track> tracks);
    void playing(Track track);
    void played();

    void paused();
    void stopped();

    void fastforwarding();
    void rewinding();

    void play();
    void next();
    void previous();


}
