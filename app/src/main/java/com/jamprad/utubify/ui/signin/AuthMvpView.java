package com.jamprad.utubify.ui.signin;

import com.jamprad.utubify.ui.base.MvpView;

/**
 * Created by jamprad on 2016-10-02.
 *
 * Interface to be called on view by sign in presenters
 */
public interface AuthMvpView extends MvpView {
}
