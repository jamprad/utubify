package com.jamprad.utubify.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jamprad.utubify.R;
import com.jamprad.utubify.injection.component.FragmentComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpotifyAuthFragment extends AuthFragment {

    @BindView(R.id.button_spotify_sign_in) Button mSpotifySignInButton;

    @Inject
    SpotifyAuthPresenter mPresenter;

    public SpotifyAuthFragment() {
        // Required empty public constructor
    }

    public static SpotifyAuthFragment newInstance(){
        SpotifyAuthFragment fragment = new SpotifyAuthFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_spotify_sign_in, container, false);
        ButterKnife.bind(this, view);

        mPresenter.attachView(this);
        return view;
    }

    @OnClick(R.id.button_spotify_sign_in)
    public void signIn() {
        mPresenter.signIn(getActivity());
    }

    @Override
    public void inject(FragmentComponent component) {
        component.inject(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.signInResult(requestCode, resultCode, data);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.detachView();
    }
}
