package com.jamprad.utubify.ui.main;

import com.jamprad.utubify.ui.base.MvpView;

public interface MainMvpView extends MvpView {
    void invalidateSignInFragments();
    void invalidateLogoutMenu();
}
