package com.jamprad.utubify.ui.signin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.SignInButton;
import com.jamprad.utubify.R;
import com.jamprad.utubify.injection.component.FragmentComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoogleAuthFragment extends AuthFragment {

    @BindView(R.id.button_google_sign_in) SignInButton mGoogleSignInButton;

    @Inject
    GoogleAuthPresenter mPresenter;

    public GoogleAuthFragment() {
        // Required empty public constructor
    }

    public static GoogleAuthFragment newInstance(){
        GoogleAuthFragment fragment = new GoogleAuthFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_google_sign_in, container, false);
        ButterKnife.bind(this, view);

        mPresenter.attachView(this);
        return view;
    }

    @OnClick(R.id.button_google_sign_in)
    public void signIn() {
        mPresenter.signIn(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void inject(FragmentComponent component) {
        component.inject(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.signInResult(requestCode, resultCode, data);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.detachView();
    }
}
