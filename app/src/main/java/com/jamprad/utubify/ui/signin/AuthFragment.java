package com.jamprad.utubify.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamprad.utubify.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AuthMvpView} interface
 * to handle interaction events.
 */
// TODO Refactor, moving any shared subclass logic here
public abstract class AuthFragment extends BaseFragment implements AuthMvpView {
    public AuthFragment() {
        // Required empty public constructor
    }

    @Override
    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    @Override
    public abstract void onActivityResult(int requestCode, int resultCode, Intent data);

}
