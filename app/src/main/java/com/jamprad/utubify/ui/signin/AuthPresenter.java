package com.jamprad.utubify.ui.signin;

import com.jamprad.utubify.data.remote.auth.Auth;
import com.jamprad.utubify.ui.base.BasePresenter;

/**
 * Created by jampradinuk on 2016-10-20.
 */

public abstract class AuthPresenter extends BasePresenter<AuthMvpView> implements Auth {
}
