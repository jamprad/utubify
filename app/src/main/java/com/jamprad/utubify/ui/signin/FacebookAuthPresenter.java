package com.jamprad.utubify.ui.signin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.jamprad.utubify.data.remote.FacebookService;
import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.injection.ConfigPersistent;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by jamprad on 2016-10-02.
 */
@ConfigPersistent
public class FacebookAuthPresenter extends AuthPresenter {

    @Inject
    FacebookService mFacebook;

    @Inject
    public FacebookAuthPresenter(){
    }

    @Override
    public void signIn(FragmentActivity activity) {
        checkViewAttached();
        mFacebook.signIn(activity);
    }

    @Override
    public Observable<AuthResult> authenticate(@NonNull FragmentActivity activity) { // TODO Review, not used
        checkViewAttached();
        return mFacebook.authenticate(activity);
    }

    @Override
    public void logout() {
        mFacebook.logout();
    }

    @Override
    public void signInResult(int requestCode, int resultCode, Intent data) {
        checkViewAttached();
        mFacebook.signInResult(requestCode, resultCode, data);
    }
}
