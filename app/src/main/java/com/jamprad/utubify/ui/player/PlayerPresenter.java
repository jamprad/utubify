package com.jamprad.utubify.ui.player;

import android.support.v4.app.Fragment;

import com.jamprad.utubify.data.DataManager;
import com.jamprad.utubify.data.model.Player;
import com.jamprad.utubify.data.model.PlayerEvent;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.data.model.VideoPlayer;
import com.jamprad.utubify.data.remote.UtubifyService;
import com.jamprad.utubify.injection.ConfigPersistent;
import com.jamprad.utubify.ui.base.BasePresenter;
import com.jamprad.utubify.util.RxUtil;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jamprad on 2016-10-02.
 */
@ConfigPersistent
public class PlayerPresenter extends BasePresenter<PlayerMvpView> implements VideoPlayer, Player {

    private final DataManager mDataManager;

    @Inject
    UtubifyService mUtubify;

    private Subscription mTracksSubscription;
    private Subscription mPlayerSubscription;

    @Inject
    public PlayerPresenter(DataManager dataManager){
        mDataManager = dataManager;
    }


    @Override
    public void attachView(PlayerMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        if (mTracksSubscription != null && !mTracksSubscription.isUnsubscribed()) mTracksSubscription.unsubscribe();
        if (mPlayerSubscription != null && !mPlayerSubscription.isUnsubscribed()) mPlayerSubscription.unsubscribe();

        super.detachView();
    }

    void loadTracks() {
        checkViewAttached();
        RxUtil.unsubscribe(mTracksSubscription);
        mTracksSubscription = mDataManager.getTracks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Track>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<Track> tracks) {
                        Collections.sort(tracks);
                        getMvpView().showTracks(tracks);
                    }
                });
    }

    void start() {
        checkViewAttached();
        RxUtil.unsubscribe(mPlayerSubscription);
        mPlayerSubscription = mUtubify.start()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PlayerEvent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(PlayerEvent playerEvent) {
                        switch (playerEvent.code) {
                            case PLAYING:
                                //play, next & previous
                                getMvpView().playing(playerEvent.track);
                                break;
                            case PAUSED:
                                getMvpView().paused();
                                break;
                            case STOPPED:
                                getMvpView().stopped();
                                break;
                            case FF:
                                getMvpView().fastforwarding();
                                break;
                            case REW:
                                getMvpView().rewinding();
                                break;
                            case DELIVERED:
                                getMvpView().played();
                                break;
                        }
                    }
                });
    }

    @Override
    public void play(Track track) {
        mUtubify.play(track);
    }

    @Override
    public void pause() {
        mUtubify.pause();
    }

    @Override
    public void stop() {
        mUtubify.stop();
    }

    @Override
    public void ff() {
        mUtubify.ff();
    }

    @Override
    public void rew() {
        mUtubify.rew();
    }


    @Override
    public Fragment getVideoPlayer() {
        return mUtubify.getVideoPlayer();
    }

    @Override
    public void setFullscreen(boolean fullscreen) {
        mUtubify.setFullscreen(fullscreen);
    }
}
