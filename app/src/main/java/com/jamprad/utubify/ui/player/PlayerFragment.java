package com.jamprad.utubify.ui.player;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.jamprad.utubify.R;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.injection.component.FragmentComponent;
import com.jamprad.utubify.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jamprad on 2016-10-12.
 */
public class PlayerFragment extends BaseFragment implements PlayerMvpView {
    public final static String YOUTUBE_PLAYER_TAG = "youtube/player";

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;

    @Inject
    PlayerPresenter mPresenter;

    @Inject
    PlayerAdapter mPlayerAdapter;

    private PlayerControlsFragment mControlsFragment;

    @BindView(R.id.frame_video)
    FrameLayout mVideoFrame;
    private YouTubePlayerSupportFragment mVideoFragment;
    private boolean isPlayingVideo;

    @Inject
    public PlayerFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy(){
// YouTube doc says activities are better off managing configuration
// changes & below did not work to preserve/resume player state
//
//        if (mYouTubePlayer != null) {
//            if (getActivity().isChangingConfigurations()){
//                setRetainInstance(true);
//                mYouTubePlayer.setRetainInstance(isPlayingVideo);
//            }
//            else {
//                mYouTubePlayer.setRetainInstance(false);
//                setRetainInstance(false);
//            }
//        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
//            mYouTubePlayer = (YouTubePlayerSupportFragment) getActivity().getSupportFragmentManager().findFragmentByTag(YOUTUBE_PLAYER_TAG);
//            isPlayingVideo = true;
//        }
        View view = inflater.inflate(R.layout.fragment_player, container, false);
        ButterKnife.bind(this, view);

        mPresenter.attachView(this);

        mRecyclerView.setAdapter(mPlayerAdapter);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mVideoFragment = (YouTubePlayerSupportFragment) mPresenter.getVideoPlayer();
        getFragmentManager().beginTransaction().add(R.id.frame_video, mVideoFragment).hide(mVideoFragment).commit();

        mPresenter.start();
        mPresenter.loadTracks();

        return view;
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mPresenter.detachView(); // TODO Review
    }

    @Override
    public void inject(FragmentComponent component) {
        component.inject(this);
    }

    @Override
    public void showTracks(List<Track> tracks) {
        mPlayerAdapter.addTracks(tracks);
        mPlayerAdapter.notifyDataSetChanged();
    }

    /**
     *
     * @param track
     */
    @Override
    public void playing(Track track) {
        FragmentManager fragMngr = getActivity().getSupportFragmentManager();
        mVideoFragment = (YouTubePlayerSupportFragment) fragMngr.findFragmentById(R.id.frame_video);
        if (!(mVideoFragment instanceof YouTubePlayerSupportFragment)) return;

        if (track.service == Track.Service.YOUTUBE){
            //show video
            if (mVideoFragment.isAdded()){
                fragMngr.beginTransaction().show(mVideoFragment).commit();
            } else {
                fragMngr.beginTransaction().add(R.id.frame_video, mPresenter.getVideoPlayer()).commit();
            }
            isPlayingVideo = true;
            mVideoFragment.setRetainInstance(true);
        } else {
            //hide video
            isPlayingVideo = false;
            mVideoFragment.setRetainInstance(false);

            if (mVideoFragment.isAdded()){
                fragMngr.beginTransaction().hide(mVideoFragment).commit();
            }
        }

        mControlsFragment.playing();
        mLinearLayoutManager.scrollToPositionWithOffset(mPlayerAdapter.getPosition(track), 10); //TODO: define dimen
    }

    @Override
    public void played() {
        mPlayerAdapter.next();
    }

    @Override
    public void paused() {
        mControlsFragment.paused();
    }

    @Override
    public void stopped() {

    }

    @Override
    public void fastforwarding() {

    }

    @Override
    public void rewinding() {

    }

    @Override
    public void play() {
        mPlayerAdapter.play();
    }

    @Override
    public void next() {
//        play(mPlayerAdapter.g)
        mPlayerAdapter.next();
    }

    @Override
    public void previous() {
        mPlayerAdapter.previous();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (isPlayingVideo){
            ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (isLandscape(newConfig)){
                if (toolbar != null) toolbar.hide();
                mPresenter.setFullscreen(true);
            }
            else {
                if (toolbar != null) toolbar.show();
                mPresenter.setFullscreen(false);
            }
        }
//            WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
//            ActionBar toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
//
//            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
//                mPresenter.setFullscreen(true);
////                attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
////                if (toolbar != null) toolbar.hide();
////                getFragmentManager().beginTransaction().hide(mControlsFragment).commit();
//            } else {
//                mPresenter.setFullscreen(false);
////                attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
////                if (toolbar != null) toolbar.show();
////                getFragmentManager().beginTransaction().show(mControlsFragment).commit();
//            }

//            getActivity().getWindow().setAttributes(attrs);
    }


    private boolean isLandscape(Configuration config) {
        return config.orientation == Configuration.ORIENTATION_LANDSCAPE;
//        WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
//        return (attrs.flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) == WindowManager.LayoutParams.FLAG_FULLSCREEN;
    }

    @Override
    public void play(Track track) {
        mPresenter.play(track);
    }

    @Override
    public void pause() {
        mPresenter.pause();
    }

    @Override
    public void stop() {
        mPresenter.stop();
    }

    @Override
    public void ff() {
        mPresenter.ff();
    }

    @Override
    public void rew() {
        mPresenter.rew();
    }

    public void inject(PlayerControlsFragment controlsFragment) {
        mControlsFragment = controlsFragment;
    }
}
