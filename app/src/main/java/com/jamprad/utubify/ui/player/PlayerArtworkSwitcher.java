package com.jamprad.utubify.ui.player;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageSwitcher;

/**
 * Created by jamprad on 2016-10-22.
 */

public class PlayerArtworkSwitcher extends ImageSwitcher {

    public PlayerArtworkSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
