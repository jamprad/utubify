package com.jamprad.utubify.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.jamprad.utubify.R;
import com.jamprad.utubify.data.remote.FacebookService;
import com.jamprad.utubify.injection.component.FragmentComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FacebookAuthFragment extends AuthFragment {

    @BindView(R.id.button_spotify_sign_in) Button mSpotifySignInButton;

    @Inject
    FacebookAuthPresenter mPresenter;

    @BindView(R.id.button_facebook_sign_in)
    LoginButton loginButton;

    @Inject
    FacebookService mFacebook;

    public FacebookAuthFragment() {
        // Required empty public constructor
    }

    public static FacebookAuthFragment newInstance(){
        FacebookAuthFragment fragment = new FacebookAuthFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private CallbackManager mCallbackManager = new CallbackManager(){
        @Override
        public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
            FacebookAuthFragment.this.onActivityResult(requestCode, resultCode, data);
            return true;
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_facebook_sign_in, container, false);
        ButterKnife.bind(this, view);

        loginButton.setFragment(this);
        loginButton.registerCallback(mCallbackManager, mFacebook);

        mPresenter.attachView(this);
        return view;
    }

    @OnClick(R.id.button_spotify_sign_in)
    public void signIn() {
        mPresenter.signIn(getActivity());
    }

    @Override
    public void inject(FragmentComponent component) {
        component.inject(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
//        mPresenter.signInResult(requestCode, resultCode, data);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.detachView();
    }
}
