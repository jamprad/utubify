package com.jamprad.utubify.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.jamprad.utubify.R;
import com.jamprad.utubify.ui.base.BaseActivity;
import com.jamprad.utubify.ui.signin.GoogleAuthFragment;
import com.jamprad.utubify.ui.signin.SpotifyAuthFragment;
import com.spotify.sdk.android.authentication.LoginActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class MainActivity extends BaseActivity
        implements MainMvpView {

    private static final String EXTRA_TRIGGER_SYNC_FLAG =
            "com.jamprad.utubify.ui.main.MainActivity.EXTRA_TRIGGER_SYNC_FLAG";

    public static final int RC_GOOGLE_SIGN_IN = 9001;
    public static final int RC_SPOTIFY_SIGN_IN = LoginActivity.REQUEST_CODE;
    public static final int RC_GOOGLE_PICK_ACCOUNT = 1000;
    public static final int RC_GOOGLE_READ = 1001;

    @Inject MainPresenter mMainPresenter;

//    @BindView(R.id.toolbar)
//    Toolbar mToolbar;

//    private Menu mMenu;

    /**
     * Return an Intent to start this Activity.
     * triggerDataSyncOnCreate allows disabling the background sync service onCreate. Should
     * only be set to false during testing.
     */
    public static Intent getStartIntent(Context context, boolean triggerDataSyncOnCreate) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_TRIGGER_SYNC_FLAG, triggerDataSyncOnCreate);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent().inject(this);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        getSupportActionBar();
//        setSupportActionBar(mToolbar);

        mMainPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainPresenter.detachView();
        if(!isChangingConfigurations()){
            mMainPresenter.destroy();
        }
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mMainPresenter.onActivityResult(requestCode, resultCode, data);
    }

    private void showFragment(int container, boolean show){
        Fragment fragment = getSupportFragmentManager().findFragmentById(container);

        if (show) {
            //show
            if (fragment == null) {
                switch (container) {
                    case R.id.fragment_google_sign_in:
                        fragment = GoogleAuthFragment.newInstance();
                        break;
                    case R.id. fragment_spotify_sign_in:
                        fragment = SpotifyAuthFragment.newInstance();
                        break;
                }
            }

            if (fragment.isVisible()) return;

            if (fragment.isAdded()) {
                getSupportFragmentManager().beginTransaction().show(fragment).commitNow();
            } else {
                getSupportFragmentManager().beginTransaction().add(container, fragment).commitNow();
            }
        } else {
            //hide
            if (fragment == null) return;

            if (fragment.isAdded()){
                if (fragment.isHidden()) return;

                getSupportFragmentManager().beginTransaction().hide(fragment).commitNow();
            }
        }
    }

    private void showGoogleSignInFragment() {
        showFragment(R.id.fragment_google_sign_in, true);
    }

    public void showSpotifySignInFragment() {
        showFragment(R.id.fragment_spotify_sign_in, true);
    }

    private void hideGoogleSignInFragment() {
        showFragment(R.id.fragment_google_sign_in, false);
    }


    private void hideSpotifySignInFragment() {
        showFragment(R.id.fragment_spotify_sign_in, false);
    }

    @Override
    public void invalidateSignInFragments() {
        if (mMainPresenter.isGoogleSignedIn()){
            hideGoogleSignInFragment();
        } else showGoogleSignInFragment();

        if (mMainPresenter.isSpotifySignedIn()){
            hideSpotifySignInFragment();
        } else showSpotifySignInFragment();
    }

    @Override
    public void invalidateLogoutMenu() {
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // inflate menu from xml
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        MenuItem logoutGoogle = menu.findItem(R.id.action_logout_google);
        if (mMainPresenter.isGoogleSignedIn()) logoutGoogle.setVisible(true);
        else logoutGoogle.setVisible(false);

        MenuItem logoutSpotify = menu.findItem(R.id.action_logout_spotify);
        if (mMainPresenter.isSpotifySignedIn()) logoutSpotify.setVisible(true);
        else logoutGoogle.setVisible(false);

//        mMenu = menu;
        return true;
    }
}
