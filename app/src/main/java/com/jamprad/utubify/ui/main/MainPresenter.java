package com.jamprad.utubify.ui.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.jamprad.utubify.data.DataManager;
import com.jamprad.utubify.data.SyncService;
import com.jamprad.utubify.data.remote.GoogleService;
import com.jamprad.utubify.data.remote.SpotifyService;
import com.jamprad.utubify.data.remote.YouTubeService;
import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.data.remote.auth.AuthService;
import com.jamprad.utubify.injection.ConfigPersistent;
import com.jamprad.utubify.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainMvpView> {

    DataManager mDataManager;

    @Inject
    GoogleService mGoogle;

    @Inject
    YouTubeService mYouTube;

    @Inject
    SpotifyService mSpotify;

    private boolean signedInGoogle = false;
    private boolean signedInSpotify = false;

    private Subscription mAuthSubscription;

    @Inject
    public MainPresenter(@NonNull DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);

        mAuthSubscription = authenticate((MainActivity) getMvpView()).subscribe(new Subscriber<AuthResult>(){
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                //shouldn't happen, "errors" are reported
                Timber.e("authenticate failed: %s", e.getStackTrace().toString());
            }

            @Override
            public void onNext(AuthResult result) {
                boolean wasSignedIn = false;
                boolean isSignedIn = (result.code == AuthResult.Code.SUCCESS || result.code == AuthResult.Code.ALREADY_SIGNED_IN);

                switch (result.service) {
                    case GOOGLE:
                        wasSignedIn = signedInGoogle;
                        signedInGoogle = isSignedIn;
                        break;
                    case SPOTIFY:
                        wasSignedIn = signedInSpotify;
                        signedInSpotify = isSignedIn;
                        break;
                }

                if (result.code == AuthResult.Code.NEVER_SIGNED_IN || (!wasSignedIn && isSignedIn) || (wasSignedIn && !isSignedIn)){
                    getMvpView().invalidateSignInFragments();
                    getMvpView().invalidateLogoutMenu();
                }

                Context context = ((MainActivity) getMvpView()).getBaseContext();
                if (isSignedIn) {
                    ((MainActivity) getMvpView()).startService(SyncService.getStartIntent(context));
                }
            }
        });
    }

    @Override
    public void detachView() {
        super.detachView();
        //kill service doesn't work here because of configuration changes
    }
    
    public void destroy() {
        if (mAuthSubscription != null && !mAuthSubscription.isUnsubscribed()) mAuthSubscription.unsubscribe();

        mSpotify.destroyPlayer(); //Google and YouTube are completely auto-managed (I think!) // TODO Review
    }

    private Observable<AuthResult> authenticate(FragmentActivity activity){
        return Observable.merge(mGoogle.authenticate(activity), mSpotify.authenticate(activity));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){;
        AuthService service = null;
        switch (requestCode) {
            case MainActivity.RC_GOOGLE_PICK_ACCOUNT:
            case MainActivity.RC_GOOGLE_SIGN_IN:
            case MainActivity.RC_GOOGLE_READ:
                service = mGoogle;
            case MainActivity.RC_SPOTIFY_SIGN_IN:
                if (service == null) service = mSpotify;
                service.signInResult(requestCode, resultCode, data);
                break;
        }
    }

    public boolean isGoogleSignedIn() {
        return signedInGoogle;
    }

    public boolean isSpotifySignedIn() {
        return signedInSpotify;
    }
}
