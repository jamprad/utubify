package com.jamprad.utubify.ui.player;

/**
 * Created by jamprad on 2016-10-26.
 */
public interface PlayerControls {
    void play();
    void pause();
    void next();
    void previous();
    void ff();


    void playing();
    void paused();
}
