package com.jamprad.utubify.ui.player;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.jamprad.utubify.R;
import com.jamprad.utubify.injection.component.FragmentComponent;
import com.jamprad.utubify.ui.base.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jampradinuk on 2016-10-19.
 */

public class PlayerControlsFragment extends BaseFragment implements PlayerControls {

    @BindView(R.id.button_play_pause)
    ImageButton playPause;

    @Inject
    PlayerPresenter mPresenter;

    PlayerFragment parent;

    @Inject
    public PlayerControlsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_player_controls, container, false);
        ButterKnife.bind(this, view);
        parent = (PlayerFragment) getParentFragment(); //TODO
        parent.inject(this);
        return view;
    }

    @Override
    public void inject(FragmentComponent component) {
        component.inject(this);
    }

    @OnClick(R.id.button_play_pause)
    public void playOrPause(View v) {
        if (v.isActivated()) {
            parent.pause();
            return;
        }
        parent.play();
    }

    @OnClick(R.id.button_rew)
    public void rew() {
        mPresenter.rew();
    }

    @OnClick(R.id.button_ff)
    public void ff() {
        mPresenter.ff();
    }

    @Override
    public void playing() {
        playPause.setActivated(true);
    }

    @Override
    public void paused() {
        playPause.setActivated(false);
    }

    @Override
    public void play() {
        parent.play();
    }

    @Override
    public void pause() {
        parent.pause();
    }

    @OnClick(R.id.button_next)
    public void next() {
        parent.next();
    }

    @OnClick(R.id.button_previous)
    public void previous() {
        parent.previous();
    }
}
