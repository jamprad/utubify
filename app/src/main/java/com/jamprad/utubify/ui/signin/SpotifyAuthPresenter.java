package com.jamprad.utubify.ui.signin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.data.remote.SpotifyService;
import com.jamprad.utubify.injection.ConfigPersistent;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by jamprad on 2016-10-02.
 */
@ConfigPersistent
public class SpotifyAuthPresenter extends AuthPresenter {

    @Inject
    SpotifyService mSpotify;

    @Inject
    public SpotifyAuthPresenter(){
    }

    @Override
    public void signIn(FragmentActivity activity) {
        mSpotify.signIn(activity);
    }

    @Override
    public Observable<AuthResult> authenticate(@NonNull FragmentActivity activity) { // TODO Review, not used
        return mSpotify.authenticate(activity);
    }

    @Override
    public void logout() {
        mSpotify.logout();
    }

    @Override
    public void signInResult(int requestCode, int resultCode, Intent data) {
        mSpotify.signInResult(requestCode, resultCode, data);
    }
}
