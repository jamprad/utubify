package com.jamprad.utubify.data.model;

/**
 * Created by jamprad on 2016-10-16.
 *
 * Player interface applies to controls & services
 *
 */
public interface Player {
    void play(Track track);
    void pause();
    void stop();
    void ff();
    void rew();
}

