package com.jamprad.utubify.data.remote;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.IdentityProviders;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTubeScopes;
import com.jamprad.utubify.BuildConfig;
import com.jamprad.utubify.R;
import com.jamprad.utubify.Utubify;
import com.jamprad.utubify.data.local.PreferencesHelper;
import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.data.remote.auth.AuthService;
import com.jamprad.utubify.ui.main.MainActivity;

import rx.Single;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.google.android.gms.auth.api.Auth.CREDENTIALS_API;
import static com.google.android.gms.auth.api.Auth.GOOGLE_SIGN_IN_API;
import static com.google.android.gms.auth.api.Auth.GoogleSignInApi;

/**
 * Created by jamprad on 2016-10-08.
 */
public class GoogleService extends AuthService {

    private final static String ACCOUNT_TYPE = "com.google";
    private final static String SCOPE_YOUTUBE = "oauth2:" + YouTubeScopes.YOUTUBE + " " + YouTubeScopes.YOUTUBE_READONLY;

    final HttpTransport mTransport = AndroidHttp.newCompatibleTransport();
    final JacksonFactory mJacksonFactory = JacksonFactory.getDefaultInstance();

    private Context mContext;

    private GoogleApiClient mGoogleApiClient;

    private CredentialRequest mCredentialRequest;
    private Credential mCredential;
    private GoogleSignInAccount mAccount;
    private GoogleCredential mGoogleCredential;

    private GoogleApiClient.ConnectionCallbacks mConnectionCb = new GoogleApiClient.ConnectionCallbacks(){
        @Override
        public void onConnected(@Nullable Bundle connectionHint) {
            Timber.d("Google - onConnected");
            if (mGoogleApiClient.hasConnectedApi(CREDENTIALS_API)){
                if(!mGoogleApiClient.hasConnectedApi(GOOGLE_SIGN_IN_API)){
                    // for silent sign in we only have GOOGLE_SIGN_IN_API
                    // (for sign in we have both CREDENTIALS_API & GOOGLE_SIGN_IN_API)
                    Auth.CredentialsApi.request(mGoogleApiClient, mCredentialRequest).setResultCallback(mResultCb);
                }
            }
        }

        @Override
        public void onConnectionSuspended(int i) {
            Timber.d("Google - onConnectionSuspended");
        }
    };
    private final GoogleApiClient.OnConnectionFailedListener mConnectionFailedCb = new GoogleApiClient.OnConnectionFailedListener(){
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Timber.e("Google - onConnectionFailed");
        }
    };
    private final ResultCallback<Result> mResultCb = new ResultCallback<Result>(){
        @Override
        public void onResult(@NonNull Result result) {
            if (result.getStatus().isSuccess()){
                if (result instanceof CredentialRequestResult){
                    Timber.d("Google - Successful CredentialRequestResult");
                    onCredentialRetrieved(((CredentialRequestResult)result).getCredential());
                }
                else if (result instanceof GoogleSignInResult) {
                    Timber.d("Google - Successful GoogleSignInResult");
                    getToken(((GoogleSignInResult) result).getSignInAccount().getEmail());
                }
                else {
                    Timber.d("Google - Successful");
                }
            } else resolveResult(result.getStatus());
        }
    };

    public GoogleService(Context context){
        super();
        mContext = context;
    }

    /* AuthService */
    @Override
    protected void signIn() {
        GoogleSignInOptions gso =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(new Scope(YouTubeScopes.YOUTUBE), new Scope(YouTubeScopes.YOUTUBE_READONLY))
                        .build();

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .enableAutoManage(mSignInActivity, mConnectionFailedCb)
                .addApi(GOOGLE_SIGN_IN_API, gso)
                .addApi(CREDENTIALS_API)
                .addConnectionCallbacks(mConnectionCb)
                .build();

        mCredentialRequest = new CredentialRequest.Builder()
                .setAccountTypes(IdentityProviders.GOOGLE)
                .build();

        Intent intent = GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mSignInActivity.startActivityForResult(intent, MainActivity.RC_GOOGLE_SIGN_IN);
    }

    @Override
    protected void signInSilently() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(mConnectionCb)
                .enableAutoManage(mSignInActivity, mConnectionFailedCb)
                .addApi(CREDENTIALS_API)
                .build();

        mCredentialRequest = new CredentialRequest.Builder()
                .setAccountTypes(IdentityProviders.GOOGLE)
                .build();
    }

    @Override
    public void logout() {
        // TODO
        Toast.makeText(mContext, mContext.getString(R.string.not_implemented), Toast.LENGTH_SHORT);
    }

    @Override
    public void signInResult(int requestCode, int resultCode, Intent data){
        GoogleSignInResult result = GoogleSignInApi.getSignInResultFromIntent(data);
        int statusCode = result.getStatus().getStatusCode();
        Timber.d("Google - signInResult, statusCode: %s",  GoogleSignInStatusCodes.getStatusCodeString(statusCode));
        if (result.isSuccess()) {
            mAccount = result.getSignInAccount();
            PreferencesHelper.setGoogleAccount(mAccount.getEmail());

            mCredential = new Credential.Builder(mAccount.getEmail())
                    .setAccountType(IdentityProviders.GOOGLE)
                    .setName(mAccount.getDisplayName())
                    .setProfilePictureUri(mAccount.getPhotoUrl())
                    .build();

            Auth.CredentialsApi.save(mGoogleApiClient, mCredential).setResultCallback(mResultCb); // TODO Review - this might not be successful...
            getToken(mAccount.getEmail());
        }
    }

    @Override
    protected boolean isSignedIn() {
        if (mGoogleCredential == null) return false;
        return mGoogleCredential.getExpiresInSeconds() > 0; //TODO Review
    }

    @Override
    protected boolean hasSignedIn() {
        return PreferencesHelper.getGoogleAccount() != null;
    }

    @Override
    protected String getToken() {
        if (mGoogleCredential == null) return null;
        return mGoogleCredential.getAccessToken();
    }

    @Override
    protected AuthResult.Service getSignInResultService() {
        return AuthResult.Service.GOOGLE;
    }

    private void getToken(String email){
        getToken(new Account(email, ACCOUNT_TYPE)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleSubscriber<String>() {
                    @Override
                    public void onSuccess(String token) {
                        mGoogleCredential = new GoogleCredential.Builder()
                                .setTransport(mTransport)
                                .setJsonFactory(mJacksonFactory)
                                .setClientSecrets(BuildConfig.GOOGLE_WEB_CLIENT_ID, BuildConfig.GOOGLE_WEB_CLIENT_SECRET).build();
                        mGoogleCredential.setAccessToken(token);

                        Utubify.get(mContext).getComponent().youtube().initializeApi(mGoogleCredential); //TODO Review
                        GoogleService.this.onSuccess(new AuthResult(AuthResult.Code.SUCCESS, AuthResult.Service.GOOGLE, token));
                    }

                    @Override
                    public void onError(Throwable error) {
                        Timber.e(error.getMessage());
                    }
                });
    }

    private Single<String> getToken(final Account account){
        return Single.create(new Single.OnSubscribe<String>(){
            @Override
            public void call(SingleSubscriber<? super String> singleSubscriber) {
                try {
                    // TODO Review; GoogleAuthUtil.getToken is outdated, right?
                    singleSubscriber.onSuccess(GoogleAuthUtil.getToken(mSignInActivity, account, SCOPE_YOUTUBE));
                } catch (Exception e){
                    singleSubscriber.onError(e);
                }
            }
        });
    }

    private void onCredentialRetrieved(Credential credential) {
        String accountType = credential.getAccountType();
        if (accountType.equals(IdentityProviders.GOOGLE)) {
            // The user has previously signed in with Google Sign-In. Silently
            // sign in the user with the same ID
            // See https://developers.google.com/identity/sign-in/android/

            mGoogleApiClient.stopAutoManage(mSignInActivity);

            GoogleSignInOptions gso =
                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .setAccountName(credential.getId())
                            .requestEmail()
                            .requestScopes(new Scope(YouTubeScopes.YOUTUBE), new Scope(YouTubeScopes.YOUTUBE_READONLY))
                            .build();

            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .enableAutoManage(mSignInActivity, mConnectionFailedCb)
                    .addApi(GOOGLE_SIGN_IN_API, gso)
                    .build();

            Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient).setResultCallback(mResultCb);
        }
    }

    private void resolveResult(Status status) {
        if (status.getStatusCode() == CommonStatusCodes.RESOLUTION_REQUIRED) {
            // Prompt the user to choose a saved credential; do not show the hint
            // selector.
            try {
                status.startResolutionForResult(mSignInActivity, MainActivity.RC_GOOGLE_READ);
            } catch (IntentSender.SendIntentException e) {
                Timber.e("Google - resolveResult, STATUS: Failed to send resolution - %s", e.getMessage());
            }
        } else {
            // The user must create an account or sign in manually.
            Timber.e("Google - resolveResult, STATUS: Unsuccessful credential request.");
        }
    }
}
