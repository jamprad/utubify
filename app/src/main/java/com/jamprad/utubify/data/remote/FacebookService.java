package com.jamprad.utubify.data.remote;

import android.content.Context;
import android.content.Intent;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.data.remote.auth.AuthService;

/**
 * Created by jamprad on 2016-10-28.
 *
 * TODO: FacebookService is *very* incomplete
 *
 */
public class FacebookService extends AuthService implements FacebookCallback<LoginResult> {

    private Context mContext;

    public FacebookService(Context context){
        mContext = context;
//        FacebookSdk.sdkInitialize(context);
    }

    @Override
    protected void signIn() {

    }

    @Override
    protected void signInSilently() {

    }

    @Override
    public void logout() {

    }

    @Override
    public void signInResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected boolean isSignedIn() {
        return false;
    }

    @Override
    protected boolean hasSignedIn() {
        return false;
    }

    @Override
    protected String getToken() {
        return null;
    }

    @Override
    protected AuthResult.Service getSignInResultService() {
        return null;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }
}
