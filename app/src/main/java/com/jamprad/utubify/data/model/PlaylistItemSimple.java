package com.jamprad.utubify.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.api.client.json.GenericJson;

import java.io.IOException;

import timber.log.Timber;

/**
 * Created by jamprad on 2016-10-16.
 */

public class PlaylistItemSimple implements Parcelable {

    public String id;
    public String json;

    public static final Creator<PlaylistItemSimple> CREATOR = new Creator<PlaylistItemSimple>() {
        @Override
        public PlaylistItemSimple createFromParcel(Parcel parcel) {
            return null;
        }

        @Override
        public PlaylistItemSimple[] newArray(int i) {
            return new PlaylistItemSimple[0];
        }
    };

    public PlaylistItemSimple (@NonNull GenericJson playlistItem) {

        id = playlistItem.get("id").toString();

        json = null;
        try{
           json = playlistItem.toPrettyString();
        } catch (IOException e) {
            Timber.e(e.getMessage());
        }
        json = null;
    }

    public PlaylistItemSimple(@NonNull String json) {
        id = null;
        this.json = json;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
