package com.jamprad.utubify.data.remote;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeRequestInitializer;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.jamprad.utubify.BuildConfig;
import com.jamprad.utubify.R;
import com.jamprad.utubify.data.PlayerService;
import com.jamprad.utubify.data.model.PlayerEvent;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.data.model.VideoPlayer;
import com.jamprad.utubify.data.model.YouTubeTrack;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import timber.log.Timber;

import static com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION;
import static com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI;
import static java.lang.System.currentTimeMillis;

/**
 * Created by jamprad on 2016-10-08.
 */
public class YouTubeService implements MusicService,
        PlayerService,
        VideoPlayer,
        YouTubePlayer.PlaybackEventListener,
        YouTubePlayer.PlaylistEventListener {
    private Context mContext;

    private static YouTube youTube;

    @Singleton
    private YouTubePlayer mPlayer;
    private YouTubePlayerSupportFragment mPlayerFragment;
    private YouTubeTrack mInitialTrack;
    private YouTubeTrack mCurrentTrack;
    private Subscriber<? super PlayerEvent> mPlayerSubscriber;


    private GoogleCredential mCredential;
    final HttpTransport mTransport = AndroidHttp.newCompatibleTransport();
    final JacksonFactory mJacksonFactory = JacksonFactory.getDefaultInstance();

    public YouTubeService(Context context) {
        mContext = context;
    }

    protected void initializeApi(GoogleCredential credential){
        mCredential = credential;

        youTube = new YouTube.Builder(mTransport, mJacksonFactory, mCredential)
                .setApplicationName(mContext.getResources().getString(R.string.app_name))
                .setYouTubeRequestInitializer(new YouTubeRequestInitializer())
                .build();
    }

    private String getLikesListId(){
        // eg. https://www.googleapis.com/youtube/v3/channels?part=contentDetails&mine=true
        try {
            return youTube.channels()
                    .list("contentDetails")
                    .setMine(true)
                    .execute()
                    .getItems()
                    .get(0)
                    .getContentDetails()
                    .getRelatedPlaylists()
                    .getLikes();
        }
        catch (Exception e){
            Timber.d(e.getMessage());
        }
        return null;
    }

    private @Nullable PlaylistItemListResponse getLikes(String likesId, @Nullable String pageToken){
        try {
            return youTube.playlistItems()
                    .list("snippet")
                    .setPlaylistId(likesId)
                    .setPageToken(pageToken)
                    .execute();
        }
        catch (Exception e){
            Timber.e("YouTube - getLikes Exception: %s", e);
        }
        return null;
    }

    private Observable<List<PlaylistItem>> getLikesAsync(){
        return Observable.create(new Observable.OnSubscribe<List<PlaylistItem>>() {
            @Override
            public void call(Subscriber<? super List<PlaylistItem>> subscriber) {
                String pageToken = null;
                String listId = getLikesListId();

                PlaylistItemListResponse response;
                while((response = getLikes(listId, pageToken)) != null){
                    subscriber.onNext(response.getItems());
                    pageToken = response.getNextPageToken();
                }
            }
        });
    }

    /**
     *
     * Listen for liked tracks.
     *
     * @return Observable likes (list of Tracks)
     */
    private Observable<List<Track>> getLikedTracks() {
        return getLikesAsync().flatMap(new Func1<List<PlaylistItem>, Observable<List<Track>>>(){
            @Override
            public Observable<List<Track>> call(List<PlaylistItem> playlistItems) {
                List<Track> tracks = new ArrayList<>();
                long timestamp = currentTimeMillis();
                int i = 0;
                for (PlaylistItem playlistItem: playlistItems){
                    tracks.add(YouTubeTrack.create(playlistItem, timestamp, i));
                    i++;
                }
                return Observable.just(tracks);
            }
        });
    }

    /**
     *
     * Listen for tracks.
     * For now, only liked tracks are fetched.
     *
     * @return Observable Track lists
     */
    @Override
    public Observable<List<Track>> getTracks() {
        return getLikedTracks();
    }

    private void initializePlayer(YouTubeTrack track) {
        if (mPlayerFragment == null) mPlayerFragment = YouTubePlayerSupportFragment.newInstance();
        mInitialTrack = track;
        mPlayerFragment.initialize(BuildConfig.GOOGLE_API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                mPlayer = player;
                if (!wasRestored){
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                    player.setPlaybackEventListener(YouTubeService.this);
                    player.setPlaylistEventListener(YouTubeService.this);
                    player.setFullscreenControlFlags(FULLSCREEN_FLAG_CONTROL_ORIENTATION | FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
                    play(mInitialTrack);

                } else player.play();
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Timber.e("YouTube Player - onInitializationFailure %s", youTubeInitializationResult.name());
            }
        });
    }

    /* PlayerService */

    @Override
    public Observable<PlayerEvent> start() {
        return Observable.create(new Observable.OnSubscribe<PlayerEvent>() {
            @Override
            public void call(Subscriber<? super PlayerEvent> subscriber) {
                mPlayerSubscriber = subscriber;
            }
        });
    }

    @Override
    public void play(Track track) {
        if (track instanceof YouTubeTrack){
            if (mPlayer == null){
                initializePlayer((YouTubeTrack) track);
            } else play((YouTubeTrack) track);
        }
    }

    private void play(YouTubeTrack track) {
        if (track == null) return;
        if (track == mCurrentTrack)
            mPlayer.play();
        else mPlayer.loadVideo(track.id);
        mCurrentTrack = track;
        mInitialTrack = null;
    }

    @Override
    public void pause() {
        if (mPlayer == null) return;
        if (mPlayer.isPlaying()) {
            mPlayer.pause();
//            mCurrentTrack = null;
        }
    }

    @Override
    public void stop() {
        pause();
        mCurrentTrack = null;
    }

    @Override
    public void ff() {
        // TODO
        Toast.makeText(mContext, mContext.getString(R.string.not_implemented), Toast.LENGTH_SHORT);
    }

    @Override
    public void rew() {
        // TODO
        Toast.makeText(mContext, mContext.getString(R.string.not_implemented), Toast.LENGTH_SHORT);
    }

    /* VideoPlayer */

    @Override
    public Fragment getVideoPlayer() {
        initializePlayer(null);
        return mPlayerFragment;
    }

    @Override
    public void setFullscreen(boolean fullscreen) {
        if (mPlayer == null) return;

        mPlayer.setFullscreen(fullscreen);
    }

    /* YouTubePlayer.PlaybackEventListener */

    @Override
    public void onPlaying() {
        Timber.d("YouTube - onPlaying()");
        if (mPlayerSubscriber != null && !mPlayerSubscriber.isUnsubscribed())
            mPlayerSubscriber.onNext(new PlayerEvent(PlayerEvent.Service.YOUTUBE,
                    PlayerEvent.Code.PLAYING, mCurrentTrack));
    }

    @Override
    public void onPaused() {
        Timber.d("YouTube - onPaused()");
        if (mPlayerSubscriber != null && !mPlayerSubscriber.isUnsubscribed())
            mPlayerSubscriber.onNext(new PlayerEvent(
                    PlayerEvent.Service.SPOTIFY,
                    PlayerEvent.Code.PAUSED,
                    mCurrentTrack));
    }

    @Override
    public void onStopped() {
        Timber.d("YouTube - onStopped()");
        if (mPlayerSubscriber != null && !mPlayerSubscriber.isUnsubscribed())
            mPlayerSubscriber.onNext(new PlayerEvent(
                    PlayerEvent.Service.SPOTIFY,
                    PlayerEvent.Code.STOPPED,
                    mCurrentTrack));
    }

    @Override
    public void onBuffering(boolean b) {
        Timber.d("YouTube - onBuffering(%s)", b);
    }

    /* YouTubePlayer.PlaylistEventListener */
    //TODO
    @Override
    public void onSeekTo(int i) {
        Timber.d("YouTube - onSeekTo(%d)", i);
    }

    @Override
    public void onPrevious() {
        Timber.d("YouTube - onPrevious()");
    }

    @Override
    public void onNext() {
        Timber.d("YouTube - onNext()");
    }

    @Override
    public void onPlaylistEnded() {
        Timber.d("YouTube - onPlaylistEnded()");
    }
}
