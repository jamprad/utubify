package com.jamprad.utubify.data.model;

import android.net.Uri;

/**
 * Created by jamprad on 2016-10-13.
 */
public abstract class Track implements Comparable<Track> {

    public enum Service {
        SPOTIFY,
        YOUTUBE
    }

    public Service service;
    public String id;
    public long timestamp;
    public int queryIndex;

    public abstract int compareTo(Track track);

    @Override
    public boolean equals(Object that){
        if (that instanceof Track) return this.id.equals(((Track) that).id);
        return false;
    }

    public abstract Uri getAlbumArtwork();

    public abstract String toJson();
}
