package com.jamprad.utubify.data.remote.auth;

import android.support.annotation.Nullable;

/**
 * Created by jampradinuk on 2016-10-18.
 */
public class AuthResult {

    public enum Code {
        SUCCESS,
        ALREADY_SIGNED_IN,
        NEVER_SIGNED_IN,
        CANCELED,
        ERROR
    }

    public enum Service {
        GOOGLE,
        SPOTIFY
    }

    public final Code code;
    public final Service service;
    public final @Nullable String token;

    public AuthResult(Code code, Service service){
        this.code = code;
        this.service = service;
        this.token = null;
    }

    public AuthResult(Code code, Service service, String token){
        this.code = code;
        this.service = service;
        this.token = token;
    }
}
