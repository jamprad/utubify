package com.jamprad.utubify.data.model;

/**
 * Created by jamprad on 2016-10-25.
 */

public class PlayerEvent {

    public enum Code {
        PLAYING,
        PAUSED,
        STOPPED,
        FF,
        REW, DELIVERED,
    }

    public enum Service {
        SPOTIFY,
        YOUTUBE
    }

    public Code code;
    public Service service;

    public Track track;

    public PlayerEvent(Service service, Code code, Track track){
        this.service = service;
        this.code = code;
        this.track = track;
    }

}
