package com.jamprad.utubify.data.model;

import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by jamprad on 2016-10-15.
 */
public class SpotifyTrack extends Track {

    private final static Gson gson = new GsonBuilder().create();

    public kaaes.spotify.webapi.android.models.Track track;

    public static SpotifyTrack create(kaaes.spotify.webapi.android.models.Track track, long timestamp, int queryIndex){
        SpotifyTrack t = new SpotifyTrack();
        t.service = Service.SPOTIFY;
        t.id = track.id;
        t.track = track;
        t.timestamp = timestamp;
        t.queryIndex = queryIndex;
        return t;
    }

    @Override
    public int compareTo(Track track){
        return 0;
    }

    @Override
    public Uri getAlbumArtwork() {
       return Uri.parse(track.album.images.get(0).url);
    }

    @Override
    public String toJson() {
        return gson.toJson(track);
    }

    public static kaaes.spotify.webapi.android.models.Track fromJson(String json) {
        return gson.fromJson(json, kaaes.spotify.webapi.android.models.Track.class);
    }
}
