package com.jamprad.utubify.data;

import com.jamprad.utubify.data.model.Player;
import com.jamprad.utubify.data.model.PlayerEvent;

import rx.Observable;

/**
 * Created by jamprad on 2016-10-25.
 */

public interface PlayerService extends Player {

    /**
     *  Start listening to player events!
     *
     * @return Observable Player events
     */
    Observable<PlayerEvent> start();
}
