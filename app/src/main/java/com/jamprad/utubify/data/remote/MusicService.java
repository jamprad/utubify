package com.jamprad.utubify.data.remote;

import com.jamprad.utubify.data.model.Track;

import java.util.List;

import rx.Observable;

/**
 * Created by jamprad on 2016-10-19.
 */

public interface MusicService {
    Observable<List<Track>> getTracks();
}
