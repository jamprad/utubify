package com.jamprad.utubify.data.remote.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by jamprad on 2016-10-08.
 */

public abstract class AuthService implements Auth {
    protected FragmentActivity mSignInActivity;
    protected List<Subscriber<? super AuthResult>> mSignInSubscribers;

    public AuthService(){
        mSignInSubscribers = new ArrayList<>();
    }

    @Override
    public void signIn(@NonNull FragmentActivity activity){
        mSignInActivity = activity;
        signIn();
    }
    protected abstract void signIn();

    public void onSuccess(AuthResult result) {
        List<Subscriber<? super AuthResult>> toRemove = new ArrayList<>();
        for(Subscriber<? super AuthResult> subscriber: mSignInSubscribers){
            if (!subscriber.isUnsubscribed()) subscriber.onNext(result);
            else toRemove.add(subscriber);
        }

        for(Subscriber<? super AuthResult> subscriber: toRemove){
            mSignInSubscribers.remove(subscriber);
        }
    }

    @Override
    public Observable<AuthResult> authenticate(@NonNull FragmentActivity activity){
        mSignInActivity = activity;
        return Observable.create(new Observable.OnSubscribe<AuthResult>(){
            @Override
            public void call(Subscriber<? super AuthResult> subscriber) {
                mSignInSubscribers.add(subscriber);

                if (!isSignedIn()){
                    if (!hasSignedIn()){
                        subscriber.onNext(new AuthResult(AuthResult.Code.NEVER_SIGNED_IN, getSignInResultService()));
                        return;
                    }

                    signInSilently();
                    return;
                }

                subscriber.onNext(new AuthResult(AuthResult.Code.ALREADY_SIGNED_IN, getSignInResultService(), getToken()));
            }
        });
    }
    protected abstract void signInSilently();

    @Override
    public abstract void signInResult(int requestCode, int resultCode, Intent data); //from fragment activity

    protected abstract boolean isSignedIn();

    protected abstract boolean hasSignedIn();

    protected abstract String getToken();

    protected abstract AuthResult.Service getSignInResultService();
}
