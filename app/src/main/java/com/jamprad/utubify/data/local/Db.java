package com.jamprad.utubify.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import com.jamprad.utubify.data.model.SpotifyTrack;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.data.model.YouTubeTrack;

public class Db {
    public Db() { }

    public abstract static class TrackTable {
        public static final String TABLE_NAME = "track";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_DATA = "data";
        public static final String COLUMN_TIME = "time";
        public static final String COLUMN_IDX = "idx";

        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " TEXT PRIMARY KEY, " +
                        COLUMN_TYPE + " TINYINT NOT NULL, " +
                        COLUMN_DATA + " TEXT NOT NULL, " +
                        COLUMN_TIME + " BIGINT NOT NULL, " +
                        COLUMN_IDX + " INTEGER NOT NULL" +
                        " ); ";

        public static ContentValues toContentValues(Track track) {
            ContentValues values = new ContentValues();
            switch (track.service){
                case SPOTIFY:
                    SpotifyTrack spotifyTrack = (SpotifyTrack) track;
                    values.put(COLUMN_ID, spotifyTrack.track.id);
                    break;
                case YOUTUBE:
                    YouTubeTrack youTubeTrack = (YouTubeTrack) track;
                    values.put(COLUMN_ID, youTubeTrack.playlistItem.getId());
                    break;
            }
            values.put(COLUMN_TYPE, track.service.ordinal());
            values.put(COLUMN_DATA, track.toJson());
            values.put(COLUMN_TIME, track.timestamp);
            values.put(COLUMN_IDX, track.queryIndex);

            return values;
        }

        public static Track parseCursor(Cursor cursor) {
            String id = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ID));
            int type = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_TYPE));
            String json = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATA));
            long time = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIME));
            int idx = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_IDX));

            Track track;
            switch (Track.Service.values()[type]) {
                case SPOTIFY:
                    track = SpotifyTrack.create(SpotifyTrack.fromJson(json), time, idx);
                    break;
                case YOUTUBE:
                    track = YouTubeTrack.create(YouTubeTrack.fromJson(json), time, idx);
                    break;

                default:
                    track = null; //TODO: throw an exception?
                    break;
            }
            return track;
        }
    }
}
