package com.jamprad.utubify.data.model;

import android.support.v4.app.Fragment;

/**
 * Created by jamprad on 2016-10-25.
 */

public interface VideoPlayer extends Player {
    Fragment getVideoPlayer();
    void setFullscreen(boolean fullscreen);
}
