package com.jamprad.utubify.data.remote;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.jamprad.utubify.Utubify;
import com.jamprad.utubify.data.PlayerService;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.data.model.PlayerEvent;
import com.jamprad.utubify.data.model.VideoPlayer;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by jamprad on 2016-10-08.
 */
@Singleton
public class UtubifyService implements PlayerService, VideoPlayer {

    private Context mContext;

    @Inject
    YouTubeService mYouTube;

    @Inject
    SpotifyService mSpotify;

//    Subscription mPlayerSubscription;
    public UtubifyService(Context context){
        mContext = context;
        Utubify.get(mContext).getComponent().inject(this);
    }

    public Observable<List<Track>> getTracks() {
        return Observable.merge(mYouTube.getTracks(), mSpotify.getTracks());
    }

    @Override
    public Fragment getVideoPlayer() {
        return mYouTube.getVideoPlayer();
    }

    @Override
    public void setFullscreen(boolean fullscreen) {
        mYouTube.setFullscreen(fullscreen);
    }

    @Override
    public Observable<PlayerEvent> start() {

        Observable<PlayerEvent> spotify = mSpotify.start();
        Observable<PlayerEvent> youtube = mYouTube.start();

        return Observable.merge(spotify, youtube);
    }

    @Override
    public void play(Track track) {
        switch (track.service){
            case YOUTUBE:
                mSpotify.pause();
                mYouTube.play(track);
                break;
            case SPOTIFY:
                mYouTube.pause();
                mSpotify.play(track);
                break;
        }
    }

    @Override
    public void pause() {
        mSpotify.pause();
        mYouTube.pause();
    }

    @Override
    public void stop() {
        mSpotify.stop();
        mYouTube.stop();
    }

    @Override
    public void ff() {
        mSpotify.ff();
        mYouTube.ff();
    }

    @Override
    public void rew() {
        mSpotify.rew();
        mYouTube.rew();
    }
}
