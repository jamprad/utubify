package com.jamprad.utubify.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by jamprad on 2016-10-16.
 */

public class TrackSimple implements Parcelable {

    public String id;
    public String json;

    public static final Creator<TrackSimple> CREATOR = new Creator<TrackSimple>() {
        @Override
        public TrackSimple createFromParcel(Parcel parcel) {
            return null;
        }

        @Override
        public TrackSimple[] newArray(int i) {
            return new TrackSimple[0];
        }
    };

    public TrackSimple(@NonNull kaaes.spotify.webapi.android.models.Track track) {
        id = track.id;
        Gson gson = new GsonBuilder().create();
        json = gson.toJson(track);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
