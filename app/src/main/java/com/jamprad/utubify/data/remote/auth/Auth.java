package com.jamprad.utubify.data.remote.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import rx.Observable;

/**
 * Created by jampradinuk on 2016-10-19.
 */

public interface Auth {
    void signIn(@NonNull FragmentActivity activity); //result from signIn should be observed by authenticate
    Observable<AuthResult> authenticate(@NonNull FragmentActivity activity);
    void logout();
    void signInResult(int requestCode, int resultCode, Intent data);
}
