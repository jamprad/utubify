package com.jamprad.utubify.data.model;

import android.net.Uri;
import android.support.annotation.Nullable;

import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.Thumbnail;
import com.google.api.services.youtube.model.ThumbnailDetails;

import java.io.IOException;

import timber.log.Timber;

/**
 * Created by jamprad on 2016-10-15.
 */
public class YouTubeTrack extends Track {

    public PlaylistItem playlistItem; //TODO: Review - playlist item might not have the info we need!

    public static YouTubeTrack create(PlaylistItem playlistItem, long timestamp, int queryIndex){
        YouTubeTrack t = new YouTubeTrack();
        t.service = Service.YOUTUBE;
        t.id = playlistItem.getSnippet().getResourceId().getVideoId();
        t.playlistItem = playlistItem;
        t.timestamp = timestamp;
        t.queryIndex = queryIndex;
        return t;
    }

    @Override
    public int compareTo(Track track) {
        return 0;
    }

    @Override
    @Nullable
    public Uri getAlbumArtwork() {
        ThumbnailDetails thumbnails = playlistItem.getSnippet().getThumbnails();
        if (thumbnails == null) return null;
        Thumbnail thumbnail = thumbnails.getMaxres();
        if (thumbnail == null) thumbnail = thumbnails.getHigh();
        if (thumbnail == null) thumbnail = thumbnails.getStandard(); // TODO Review getStandard(), getDefault()
        if (thumbnail == null) thumbnail = thumbnails.getDefault();
        if (thumbnail == null) return null;
        return Uri.parse(thumbnail.getUrl());
    }

    @Override
    public String toJson() {
        try {
            return JacksonFactory.getDefaultInstance().toString(playlistItem);
        } catch (IOException e) {
            Timber.e(e.getMessage());
        }
        return null;
    }

    public static PlaylistItem fromJson(String json) {
        try {
            return JacksonFactory.getDefaultInstance().fromString(json, PlaylistItem.class);
        } catch (IOException e) {
            Timber.e(e.getMessage());
        }
        return null;
    }
}