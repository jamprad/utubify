package com.jamprad.utubify.data.remote;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.jamprad.utubify.BuildConfig;
import com.jamprad.utubify.R;
import com.jamprad.utubify.data.PlayerService;
import com.jamprad.utubify.data.local.PreferencesHelper;
import com.jamprad.utubify.data.model.SpotifyTrack;
import com.jamprad.utubify.data.remote.auth.AuthResult;
import com.jamprad.utubify.data.remote.auth.AuthService;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.PlayerEvent;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.SpotifyPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.models.Pager;
import kaaes.spotify.webapi.android.models.Track;
import retrofit.client.Response;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import timber.log.Timber;

import static com.spotify.sdk.android.authentication.AuthenticationResponse.Type.TOKEN;
import static com.spotify.sdk.android.authentication.LoginActivity.REQUEST_CODE;

/**
 * Created by jamprad on 2016-10-08.
 */
public class SpotifyService extends AuthService implements MusicService,
        PlayerService {
    private static final String REDIRECT_URI = "utubify://spotify/auth";

    private Context mContext;

    private String mToken;

    /* DATA */
    private SpotifyApi mApi;
    private kaaes.spotify.webapi.android.SpotifyService mSpotify;

    /* PLAYER */
    private com.spotify.sdk.android.player.Player mPlayer;
    private SpotifyTrack mCurrentTrack;
    private SpotifyTrack mInitialTrack;
    private Subscriber<? super com.jamprad.utubify.data.model.PlayerEvent> mPlayerSubscriber;

    private final ConnectionStateCallback mConnectionStateCb = new ConnectionStateCallback() {
        @Override
        public void onLoggedIn() {
            Timber.i("Spotify - User logged in");
        }

        @Override
        public void onLoggedOut() {
            Timber.i("Spotify - User logged out");
        }

        @Override
        public void onLoginFailed(int i) {
            Timber.e("Spotify - login failed %d", i);
        }

        @Override
        public void onTemporaryError() {
            Timber.d("Spotify - Temporary error occurred");
        }

        @Override
        public void onConnectionMessage(String s) {
            Timber.d("Received connection message: %s", s);
        }
    };
    private final SpotifyPlayer.NotificationCallback mNotificationCb = new SpotifyPlayer.NotificationCallback(){
        @Override
        public void onPlaybackEvent(PlayerEvent playerEvent) {
            Timber.d("Spotify - Playback event received: " + playerEvent.name());
            switch (playerEvent) {
                // Handle event type as necessary
                case UNKNOWN:
                    break;
                case kSpPlaybackNotifyPlay:
                    if (mPlayerSubscriber != null && !mPlayerSubscriber.isUnsubscribed())
                        mPlayerSubscriber.onNext(new com.jamprad.utubify.data.model.PlayerEvent(
                                com.jamprad.utubify.data.model.PlayerEvent.Service.SPOTIFY,
                                com.jamprad.utubify.data.model.PlayerEvent.Code.PLAYING,
                                mCurrentTrack));
                    break;
                case kSpPlaybackNotifyPause:
                    if (mPlayerSubscriber != null && !mPlayerSubscriber.isUnsubscribed())
                        mPlayerSubscriber.onNext(new com.jamprad.utubify.data.model.PlayerEvent(
                                com.jamprad.utubify.data.model.PlayerEvent.Service.SPOTIFY,
                                com.jamprad.utubify.data.model.PlayerEvent.Code.PAUSED,
                                mCurrentTrack));
                    break;
                case kSpPlaybackNotifyTrackChanged:
                    break;
                case kSpPlaybackNotifyNext:
                    break;
                case kSpPlaybackNotifyPrev:
                    break;
                case kSpPlaybackNotifyShuffleOn:
                    break;
                case kSpPlaybackNotifyShuffleOff:
                    break;
                case kSpPlaybackNotifyRepeatOn:
                    break;
                case kSpPlaybackNotifyRepeatOff:
                    break;
                case kSpPlaybackNotifyBecameActive:
                    break;
                case kSpPlaybackNotifyBecameInactive:
                    break;
                case kSpPlaybackNotifyLostPermission:
                    break;
                case kSpPlaybackEventAudioFlush:
                    break;
                case kSpPlaybackNotifyAudioDeliveryDone:
                    if (mPlayerSubscriber != null && !mPlayerSubscriber.isUnsubscribed())
                        mPlayerSubscriber.onNext(new com.jamprad.utubify.data.model.PlayerEvent(
                                com.jamprad.utubify.data.model.PlayerEvent.Service.SPOTIFY,
                                com.jamprad.utubify.data.model.PlayerEvent.Code.DELIVERED,
                                mCurrentTrack));
                    break;
                case kSpPlaybackNotifyContextChanged:
                    break;
                case kSpPlaybackNotifyTrackDelivered:
                    break;
                case kSpPlaybackNotifyMetadataChanged:
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onPlaybackError(Error error) {
            Timber.d("Playback error received: %s", error.name());
            switch (error) {
                // Handle error type as necessary
                default:
                    break;
            }
        }
    };
    private final SpotifyPlayer.InitializationObserver mInitializationObs = new SpotifyPlayer.InitializationObserver(){
        @Override
        public void onInitialized(SpotifyPlayer spotifyPlayer) {
            mPlayer = spotifyPlayer;
            mPlayer.addConnectionStateCallback(mConnectionStateCb);
            mPlayer.addNotificationCallback(mNotificationCb);
            if (mInitialTrack != null) play(mInitialTrack);
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Could not initializeFragment fragment_player: " + throwable.getMessage());
        }
    };
    private final SpotifyPlayer.OperationCallback mOperationCallback = new SpotifyPlayer.OperationCallback() {
        @Override
        public void onSuccess() {
            Timber.d("Spotify - operation success");
        }

        @Override
        public void onError(Error error) {
            Timber.e("Spotify - operation error %s", error);
        }
    };

    public SpotifyService(Context context){
        super();
        mContext = context;
    }

    /* AuthService */

    @Override
    protected void signIn() {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(BuildConfig.SPOTIFY_CLIENT_ID,
                TOKEN,
                REDIRECT_URI);
        builder.setScopes(new String[]{"user-read-private", "user-top-read", "streaming"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(mSignInActivity, REQUEST_CODE, request);
    }

    @Override
    protected void signInSilently() {
        signIn();
    }

    @Override
    public void logout() {
        // TODO
        Toast.makeText(mContext, mContext.getString(R.string.not_implemented), Toast.LENGTH_SHORT);
    }

    @Override
    public void signInResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE){
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);

            AuthResult.Code code = AuthResult.Code.ERROR;
            AuthResult.Service service = AuthResult.Service.SPOTIFY;
            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    // Handle successful response
                    mToken = response.getAccessToken();

                    mApi = new SpotifyApi();
                    mApi.setAccessToken(mToken);
                    mSpotify = mApi.getService();

                    Config playerConfig = new Config(mContext, mToken, BuildConfig.SPOTIFY_CLIENT_ID);
                    mPlayer = Spotify.getPlayer(playerConfig, SpotifyService.this, mInitializationObs);

                    code = AuthResult.Code.SUCCESS;

                    break;

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    break;

                // Most likely auth flow was cancelled
                default:
                    code = AuthResult.Code.CANCELED;
                    // Handle other cases

            }

            onSuccess(new AuthResult(code, service, mToken));
        }
    }

    @Override
    public boolean isSignedIn() {
        return (mToken != null); //TODO: Review
    }

    @Override
    protected boolean hasSignedIn() {
        return PreferencesHelper.getSpotifyAccount() != null;
    }

    @Override
    protected String getToken() {
        return mToken;
    }

    @Override
    protected AuthResult.Service getSignInResultService() {
        return AuthResult.Service.SPOTIFY;
    }

    /* MusicService */

    @Override
    public Observable<List<com.jamprad.utubify.data.model.Track>> getTracks() {
        return getTopTracks();
    }

    private Observable<List<com.jamprad.utubify.data.model.Track>> getTopTracks(){
        return getTopTracksAsync().flatMap(new Func1<List<Track>, Observable<List<com.jamprad.utubify.data.model.Track>>>(){
            @Override
            public Observable<List<com.jamprad.utubify.data.model.Track>> call(List<Track> spotifyTracks) {
                List<com.jamprad.utubify.data.model.Track> tracks = new ArrayList<>();
                long timestamp = System.currentTimeMillis();
                int i = 0;
                for (Track track : spotifyTracks){
                    tracks.add(SpotifyTrack.create(track, timestamp, i));
                    i++;
                }
                return Observable.just(tracks);
            }
        });
    }

    private Observable<List<Track>> getTopTracksAsync(){
        return Observable.create(new Observable.OnSubscribe<List<Track>>(){
            private final List<Subscriber<? super List<Track>>> mTrackListSubscribers = new ArrayList<>();
            private final SpotifyCallback<Pager<Track>> mTrackPagerCallback = new SpotifyCallback<Pager<Track>>() {
                @Override
                public void failure(SpotifyError spotifyError) {
                    Timber.e("Spotify - failure %s", spotifyError.getMessage());
                }

                @Override
                public void success(Pager<Track> trackPager, Response response) {
                    for (Subscriber<? super List<Track>> subscriber : mTrackListSubscribers){
                        if (subscriber.isUnsubscribed()) continue;
                        subscriber.onNext(trackPager.items);
                    }

                    Map<String, Object> options = new HashMap<>();
                    options.put(kaaes.spotify.webapi.android.SpotifyService.OFFSET, trackPager.offset + trackPager.items.size());

                    if (trackPager.offset == trackPager.total) return;
                    mSpotify.getTopTracks(options, this);
                }
            };

            @Override
            public void call(final Subscriber<? super List<Track>> subscriber) {
                mTrackListSubscribers.add(subscriber);
                mSpotify.getTopTracks(mTrackPagerCallback);
            }
        });
    }

    /* PlayerService*/

    @Override
    public Observable<com.jamprad.utubify.data.model.PlayerEvent> start() {
        return Observable.create(new Observable.OnSubscribe<com.jamprad.utubify.data.model.PlayerEvent>() {
            @Override
            public void call(Subscriber<? super com.jamprad.utubify.data.model.PlayerEvent> subscriber) {
                mPlayerSubscriber = subscriber;
            }
        });
    }

    @Override
    public void play(com.jamprad.utubify.data.model.Track track) {
        if (track instanceof SpotifyTrack){
            if (mPlayer == null){
                initializePlayer((SpotifyTrack) track);
            }
            else {
                play((SpotifyTrack) track);
            }
        }
    }

    @Override
    public void pause() {
        if (mPlayer == null) return;
        if (mPlayer.getPlaybackState().isPlaying)
            mPlayer.pause(mOperationCallback);
    }

    @Override
    public void stop() {
        pause();
    }

    @Override
    public void ff() {
        // TODO
        Toast.makeText(mContext, mContext.getString(R.string.not_implemented), Toast.LENGTH_SHORT);
    }

    @Override
    public void rew() {
        // TODO
        Toast.makeText(mContext, mContext.getString(R.string.not_implemented), Toast.LENGTH_SHORT);
    }

    private void play(@NonNull SpotifyTrack track){
        if (mCurrentTrack == track) mPlayer.resume(mOperationCallback);
        else mPlayer.playUri(mOperationCallback, "spotify:track:" + track.track.id, 0, 0);

        mCurrentTrack = track;
    }

    private void initializePlayer(@Nullable SpotifyTrack track) {
        mInitialTrack = track;
        Config playerConfig = new Config(mContext, mToken, BuildConfig.SPOTIFY_CLIENT_ID);
        Spotify.getPlayer(playerConfig, SpotifyService.this, mInitializationObs);
    }

    /**
     * Destroys Spotify player
     */
    public void destroyPlayer() {
        if (mPlayer != null) Spotify.destroyPlayer(mPlayer);
    }

}
