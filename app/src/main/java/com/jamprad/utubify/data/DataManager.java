package com.jamprad.utubify.data;

import com.jamprad.utubify.data.local.DatabaseHelper;
import com.jamprad.utubify.data.local.PreferencesHelper;
import com.jamprad.utubify.data.model.Track;
import com.jamprad.utubify.data.remote.UtubifyService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.functions.Func1;

@Singleton
public class DataManager {

    private final UtubifyService mUtubify;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public DataManager(UtubifyService utubifyService, PreferencesHelper preferencesHelper,
                       DatabaseHelper databaseHelper) {
        mUtubify = utubifyService;
        mPreferencesHelper = preferencesHelper;
        mDatabaseHelper = databaseHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public Observable<Track> syncTracks() {
                return mUtubify.getTracks()
                        .flatMap(new Func1<List<Track>, Observable<Track>>() {
                            @Override
                    public Observable<Track> call(List<Track> tracks) {
                                /*
                                return mDatabaseHelper.addTracks(tracks);
                                /*/
                                return mDatabaseHelper.updateTracks(tracks);
                                //*/
                    }
                });
    }

    public Observable<List<Track>> getTracks() {
        return mDatabaseHelper.getTracks().distinct();
    }
}
