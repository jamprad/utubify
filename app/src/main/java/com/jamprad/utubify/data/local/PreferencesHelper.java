package com.jamprad.utubify.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.jamprad.utubify.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {

    public static final String PREF_FILE_NAME = "utubify_pref_file";

    private static final String GOOGLE_ACCOUNT = "google_account";
    private static final String SPOTIFY_ACCOUNT = "spotify_account";

    private final SharedPreferences mPref;

    private static PreferencesHelper sInstance;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        sInstance = this;
    }

    public static @Nullable String getSpotifyAccount(){
        return sInstance.mPref.getString(GOOGLE_ACCOUNT, null);
    }

    public static void setSpotifyAccount(String account){
        sInstance.mPref.edit().putString(SPOTIFY_ACCOUNT, account).apply();
    }

    public static @Nullable String getGoogleAccount(){
        return sInstance.mPref.getString(GOOGLE_ACCOUNT, null);
    }

    public static void setGoogleAccount(String account){
        sInstance.mPref.edit().putString(GOOGLE_ACCOUNT, account).apply();
    }

    public static void clear() {
        sInstance.mPref.edit().clear().apply();
    }

}
