package com.jamprad.utubify.test.common.injection.component;

import com.jamprad.utubify.injection.component.ApplicationComponent;
import com.jamprad.utubify.test.common.injection.module.ApplicationTestModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
