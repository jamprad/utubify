package com.jamprad.utubify.test.common.injection.module;

import android.app.Application;
import android.content.Context;

import com.jamprad.utubify.data.DataManager;
import com.jamprad.utubify.data.remote.FacebookService;
import com.jamprad.utubify.data.remote.GoogleService;
import com.jamprad.utubify.data.remote.SpotifyService;
import com.jamprad.utubify.data.remote.UtubifyService;
import com.jamprad.utubify.data.remote.YouTubeService;
import com.jamprad.utubify.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

/**
 * Provides application-level dependencies for an app running on a testing environment
 * This allows injecting mocks if necessary.
 */
@Module
public class ApplicationTestModule {

    private final Application mApplication;

    public ApplicationTestModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    /************* MOCKS *************/

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return mock(DataManager.class);
    }

//    @Provides
//    @Singleton
//    RibotsService provideRibotsService() {
//        return mock(RibotsService.class);
//    }

    @Provides
    @Singleton
    UtubifyService provideUtubifyService() {
        return mock(UtubifyService.class);
    }

    @Provides
    @Singleton
    GoogleService provideGoogleService() {return mock(GoogleService.class);}

    @Provides
    @Singleton
    SpotifyService provideSpotifyService() {return mock(SpotifyService.class);}

    @Provides
    @Singleton
    YouTubeService provideYouTubeService() {return mock(YouTubeService.class);}

    @Provides
    @Singleton
    FacebookService provideFacebookService() {return mock(FacebookService.class);}




}
